# EmuBuild - SameBoy

The [SameBoy open source Game Boy emulator repository](https://github.com/LIJI32/SameBoy) only provides precompiled binaries for Windows and MacOS. This repository aims to provide always up-to-date builds for Linux and RetroArch (libretro core), as well as a script to easily compile it yourself.

## Automatic releases

A pipeline is run daily to check and see if there is a new release in the official SameBoy repository, and to build the abovementioned targets if necessary. The newest release can be found [here, under "Packages"](https://gitlab.com/avLightless/emubuild-sameboy/-/releases). The provided archive contains the native linux binary, the RetroArch libretro core, and the bootroms built using `CONF=release`.

**Note**: For RetroArch I recommend downloading the SameBoy core from the built-in downloader, and replacing the file with the one from the zip file. You can find out where your RetroArch stores cores by checking the settings. To provide information about the core RetroArch uses a `.info` file in a separate location. If you don't change this file, RetroArch won't show the new version number for the core, but this will have no effect on the working. I have also provided a `sameboy_libretro.info` file if you don't wish to use the built-in core downloader, but you will have to change the version number yourself.

## Build it yourself (easy)

**Dependencies:**
- git
- docker (edit `run.sh` to use an alternative)

Open a terminal and run the following command. It will clone this repository and run `run.sh`.

```sh
git clone 'https://gitlab.com/avLightless/emubuild-sameboy.git' && cd emubuild-sameboy && ./run.sh
```

This will make a directory called `output` inside `emubuild-sameboy` where you can find all your files.

Unless something drastically changes in the way SameBoy is compiled, you can just run `run.sh` again without cloning the repo. It will always download and build the most up to date version.
