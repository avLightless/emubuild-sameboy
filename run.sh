#! /usr/bin/env bash

# Target directory
OUTPUTDIR="$PWD"/output

# Check if OUTPUTDIR exists. Empty it if true, create it if false.
{ [ -d "$OUTPUTDIR"  ] && [ -n "$(ls -A $OUTPUTDIR)" ] && rm -r -v "$OUTPUTDIR"/* ; } || mkdir -p "$OUTPUTDIR"

# Build image and run container
docker build -t sameboy-builder - < Dockerfile && docker run --rm -it -v "$OUTPUTDIR:/app" sameboy-builder
